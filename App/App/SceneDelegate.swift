//
//  SceneDelegate.swift
//  App
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Core
import Login
import Home
import History
import Register
import OTP
import CreatePIN
import PINSuccess
import Contact
import CreateTransfer
import ConfirmTransfer
import PINTransfer
import SuccessTransfer
import FailedTransfer
import Topup
import Profile
import Personal

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        self.setupAppRouter()
        
        guard let scene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = scene
        
        self.reloadRootView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadRootView), name: Notification.Name("reloadRootView"), object: nil)
    }
    
    @objc func reloadRootView() {
        let token: String? = UserDefaultHelper.shared.get(key: .userToken)
        if token != nil {
            AppRouter.shared.navigateToHome()
        }
        else {
            AppRouter.shared.navigateToLogin()
        }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}

@available(iOS 13.0, *)
extension SceneDelegate {
    func setupAppRouter() {
        AppRouter.shared.loginScene = {
            LoginRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.homeScene = {
            HomeRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.historyScene = {
            HistoryRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.registerScene = {
            RegisterRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.otpScene = {
            OTPRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.createPINScene = {
            CreatePINRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.pinSuccessScene = {
            PINSuccessRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.contactScene = {
            ContactRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.createTransferScene = {
            CreateTransferRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.confirmTransferScene = {
            ConfirmTransferRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.pinTransferScene = {
            PINTransferRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.successTransferScene = {
            SuccessTransferRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.failedTransferScene = {
            FailedTransferRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.topupScene = {
            TopupRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.profileScene = {
            ProfileRouterImpl.navigateToModule()
        }
        
        AppRouter.shared.personalScene = {
            PersonalRouterImpl.navigateToModule()
        }
    }
}
