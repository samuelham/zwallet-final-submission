![Cocoapods platforms](https://img.shields.io/cocoapods/p/LFAlertController) ![swift-version](https://img.shields.io/badge/swift-5.0-brightgreen.svg)

# ZWallet
Your easy e-wallet app for peer-to-peer transactional purpose

## Features

ZWallet-App have 5 features:
1. Signup and Login
2. Update profile
3. Transfer
4. Topup
5. History

### Signup and Login

The signup feature is used to register a new account and create a PIN.
| Feature step | Signup | Input OTP | Login | Create PIN | Success |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Signup | <img src="/Image/signup.png" height= "280" width= "150" /> | <img src="/Image/otp-pin.png" height= "280" width= "150" /> | <img src="/Image/login.png" height= "280" width= "150" /> | <img src="/Image/create-pin.png" height= "280" width= "150" /> | <img src="/Image/pin-created.png" height= "280" width= "150" /> |

### Update Profile

The Update Profile feature is used to add or update user data.
| Feature step | Homepage | Profile | Personal Info |
| ------ | ------ | ------ | ------ |
| Update Profile | <img src="/Image/homepage.png" height= "280" width= "150" /> | <img src="/Image/profile.png" height= "280" width= "150" /> | <img src="/Image/personal-info.png" height= "280" width= "150" /> | 

### Transfer

The transfer feature is used to send money to other users using the mobile number in the contact.
| Feature step | Create Transfer | Detail Transfer | PIN  Confirmation | Finish |
| ------ | ------ | ------ | ------ | ------ |
| Transfer | <img src="/Image/create-transfer.png" height= "280" width= "150" /> | <img src="/Image/detail-transfer.png" height= "280" width= "150" /> | <img src="/Image/pin-transfer.png" height= "280" width= "150" /> | <img src="/Image/success-transfer.png" height= "280" width= "150" /> |

### Topup

The Topup feature is used to add user balances.
| Feature step | Homepage | Profile |
| ------ | ------ | ------ | 
| Topup | <img src="/Image/homepage.png" height= "280" width= "150" /> | <img src="/Image/topup.png" height= "280" width= "150" /> | 

### History

The History feature is used to show transaction history.
| Feature step | Homepage |
| ------ | ------ |
| History | <img src="/Image/homepage.png" height= "280" width= "150" /> | 

## Requirements
- iOS 12.1+
- Xcode 12.4
- Swift 5.0

## Installation

### Clone Git
```
 https://gitlab.com/samuelham/zwallet-final-submission.git
```

### Download Git
```
1. Download zip from https://gitlab.com/samuelham/zwallet-final-submission.git
2. Extract zip
```

## Library

Use CocoaPods to install Third-party Library

```
platform :ios, '11.0'
use_frameworks!
pod 'Kingfisher'
```

```
platform :ios, '11.0'
use_frameworks!
pod 'Moya'
```

## Usage Instruction

### How to Run App

1. Go to ```/App```folder in terminal
2. Run ``` Pod Install ```
3. Open App > ZWallet.xcworkspace
4. Run project and check iPhone simulator
5. Finish

## Usage Details

This application implemented Viper design pattern. Viper is a clean architecture design pattern for iOS applications. Viper stands for View, Interactor, Presenter, Entity, and Router. Viper is called a clean architecture because it divides the application logic according to its purpose.

- View: The layer responsible for implementing the user interface
- Interactor: The layer for implementing business logic
- Presenter: The layer that is responsible for collecting data that Interactor needs
- Entity: The layer to accommodate model / object data
- Router: The layer for managing page routing

One of the advantages of Viper is that it is easy to test applications. Compared to the MVC design pattern, app logic tends not to be implemented in the Model or in the View, but in the controller. This resulted in the emergence of a large controller, so that it became a challenge in itself to simplify the existing view controller.





