//
//  ConfirmTransferRouterImpl.swift
//  ConfirmTransfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

public class ConfirmTransferRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.ConfirmTransfer")
        let vc = ConfirmTransferViewController(nibName: "ConfirmTransferViewController", bundle: bundle)
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
