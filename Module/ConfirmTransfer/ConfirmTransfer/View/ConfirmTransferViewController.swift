//
//  ConfirmTransferViewController.swift
//  ConfirmTransfer
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core
import Kingfisher

class ConfirmTransferViewController: UIViewController {
    
    @IBOutlet weak var imgContact: UIImageView!
    
    @IBOutlet weak var nameContact: UILabel!
    
    @IBOutlet weak var phoneContact: UILabel!
    
    @IBOutlet weak var amountLbl: UILabel!
    
    @IBOutlet weak var balanceLeftLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    
    @IBOutlet weak var timeLbl: UILabel!
    
    @IBOutlet weak var notesLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDataSetup()

        // Do any additional setup after loading the view.
    }
    
    func labelDataSetup() {
        let getContactName: String = UserDefaultHelper.shared.get(key: .contactName) ?? ""
        nameContact.text = getContactName
        
        let getContactPhone: String = UserDefaultHelper.shared.get(key: .contactPhone) ?? ""
        phoneContact.text = getContactPhone
        
        let getContactImage: String = UserDefaultHelper.shared.get(key: .contactImage) ?? ""
        let url = URL(string: getContactImage)
        imgContact.kf.setImage(with: url)
        
        let getAmountTransfer: String = UserDefaultHelper.shared.get(key: .amountTransfer) ?? ""
        amountLbl.text = Int(getAmountTransfer)?.formatToIdr()
        
        let getNotesTransfer: String = UserDefaultHelper.shared.get(key: .notesTransfer) ?? ""
        notesLbl.text = getNotesTransfer
        
        let getCurrentBalance: String = UserDefaultHelper.shared.get(key: .currentBalance) ?? ""
        balanceLeftLbl.text = (Int(getCurrentBalance)! - Int(getAmountTransfer)!).formatToIdr()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.locale = .current
        formatter.dateFormat = "dd MMMM yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.timeZone = .current
        timeFormatter.locale = .current
        timeFormatter.dateFormat = "HH:mm"
        
        dateLbl.text = formatter.string(from: date)
        timeLbl.text = timeFormatter.string(from: date)
    }
    
    @IBAction func onClickConfirm(_ sender: Any) {
        AppRouter.shared.navigateToPINTransfer()
    }
    
    @IBAction func onClickBackCreateTransfer(_ sender: Any) {
        AppRouter.shared.navigateToCreateTransfer()
    }
    
    

}
