//
//  TransactionCell.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Kingfisher

public class TransactionCell: UITableViewCell {

    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var noteLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func showData(transaction: TransactionEntity){
        self.nameLabel.text = transaction.name
        self.noteLabel.text = transaction.notes
        
        if transaction.type == "in" {
            self.priceLabel.text = "+\(transaction.amount.formatToIdr())"
            self.priceLabel.textColor = .green
        } else {
            self.priceLabel.text = "-\(transaction.amount.formatToIdr())"
            self.priceLabel.textColor = .red
        }
        
        let url = URL(string: transaction.imageUrl)
        self.userImage.kf.setImage(with: url)
    }
}
