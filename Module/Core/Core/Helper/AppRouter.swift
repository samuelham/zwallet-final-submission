//
//  AppRouter.swift
//  Core
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import UIKit

public class AppRouter {
    public static let shared: AppRouter = AppRouter()
    
    public var loginScene: (() -> ())? = nil
    public func navigateToLogin(){
        self.loginScene?()
    }
    
    public var homeScene: (() -> ())? = nil
    public func navigateToHome() {
        self.homeScene?()
    }
    
    public var registerScene: (() -> ())? = nil
    public func navigateToRegister() {
        self.registerScene?()
    }
    
    public var historyScene: (() -> ())? = nil
    public func navigateToHistory(_ viewController: UIViewController) {
        self.historyScene?()
    }
    
    public var otpScene: (() -> ())? = nil
    public func navigateToOTP() {
        self.otpScene?()
    }
    
    public var createPINScene: (() -> ())? = nil
    public func navigateToCreatePIN() {
        self.createPINScene?()
    }
    
    public var pinSuccessScene: (() -> ())? = nil
    public func navigateToPINSuccess() {
        self.pinSuccessScene?()
    }
    
    public var contactScene: (() -> ())? = nil
    public func navigateToContact() {
        self.contactScene?()
    }
    
    public var createTransferScene: (() -> ())? = nil
    public func navigateToCreateTransfer() {
        self.createTransferScene?()
    }
    
    public var confirmTransferScene: (() -> ())? = nil
    public func navigateToConfirmTransfer() {
        self.confirmTransferScene?()
    }
    
    public var pinTransferScene: (() -> ())? = nil
    public func navigateToPINTransfer() {
        self.pinTransferScene?()
    }
    
    public var successTransferScene: (() -> ())? = nil
    public func navigateToSuccessTransfer() {
        self.successTransferScene?()
    }
    
    public var failedTransferScene: (() -> ())? = nil
    public func navigateToFailedTransfer() {
        self.failedTransferScene?()
    }
    
    public var topupScene: (() -> ())? = nil
    public func navigateToTopup() {
        self.topupScene?()
    }
    
    public var profileScene: (() -> ())? = nil
    public func navigateToProfile() {
        self.profileScene?()
    }
    
    public var personalScene: (() -> ())? = nil
    public func navigateToPersonal() {
        self.personalScene?()
    }
}
