//
//  ProfileNetworkManager.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public protocol ProfileNetworkManager {
    func getProfile(completion: @escaping (ProfileDataResponse?, Error?) -> ())
}
