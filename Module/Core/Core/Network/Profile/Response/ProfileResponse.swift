//
//  ProfileResponse.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public struct ProfileResponse: Codable {
    var status: Int
    var message: String
    var data: ProfileDataResponse
}

public struct ProfileDataResponse: Codable {
    public var firstname: String
    public var lastname: String
    public var email: String
    public var phone: String
    public var image: String
}
