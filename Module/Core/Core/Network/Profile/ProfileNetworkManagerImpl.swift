//
//  ProfileNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Moya

public class ProfileNetworkManagerImpl: ProfileNetworkManager {
    
    public init(){
        
    }
    
    public func getProfile(completion: @escaping (ProfileDataResponse?, Error?) -> ()) {
        let provider = MoyaProvider<ProfileApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.getProfile) { response in
            switch response{
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getProfileResponse = try decoder.decode(ProfileResponse.self, from: result.data)
                    completion(getProfileResponse.data, nil)
                }
                catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
