//
//  ProfileApi.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Moya

public enum ProfileApi {
    case getProfile
}

extension ProfileApi: TargetType {
    public var path: String{
        switch self {
        case .getProfile:
            return "/user/myProfile"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self{
        case .getProfile:
            return .requestPlain
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        return [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(token)"
        ]
    }
}
