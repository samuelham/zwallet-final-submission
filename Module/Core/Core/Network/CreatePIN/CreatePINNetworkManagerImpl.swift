//
//  CreatePINNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public class CreatePINNetworkManagerImpl: CreatePINNetworkManager {
    
    public init(){
        
    }
    
    public func getPIN(pin: String, completion: @escaping (CreatePINResponse?, Error?) -> ()) {
        let provider = MoyaProvider<CreatePINApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.getCreatePINApiResponse(pin: pin)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let createPINResponse = try decoder.decode(CreatePINResponse.self, from: result.data)
                    completion(createPINResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
