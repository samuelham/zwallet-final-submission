//
//  CreatePINApi.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public enum CreatePINApi {
    case getCreatePINApiResponse(pin: String)
}

extension CreatePINApi: TargetType {
    public var path: String {
        switch self {
        case .getCreatePINApiResponse:
            return "/auth/PIN"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getCreatePINApiResponse(let pin):
            return .requestParameters(parameters: ["PIN":pin], encoding: JSONEncoding.default)
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .patch
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        return [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(token)"
        ]
    }
}
