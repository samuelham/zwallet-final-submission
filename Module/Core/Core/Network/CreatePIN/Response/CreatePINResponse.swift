//
//  CreatePINResponse.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public struct CreatePINResponse: Codable {
    public var status: Int
    public var message: String
}
