//
//  TopupNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Moya

public class TopupNetworkManagerImpl: TopupNetworkManager{
    
    public init() {
        
    }
    
    public func topup(phone: String, amount: String, completion: @escaping (TopupResponse?, Error?) -> ()) {
        let provider = MoyaProvider<TopupApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.patchTopup(phone: phone, amount: amount)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let topupResponse = try decoder.decode(TopupResponse.self, from: result.data)
                    completion(topupResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
