//
//  TopupApi.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Moya

public enum TopupApi {
    case patchTopup(phone: String, amount: String)
}

extension TopupApi: TargetType {
    public var path: String{
        switch self {
        case .patchTopup:
            return "/topup/topupbalance"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self{
        case .patchTopup(let phone, let amount):
            return .requestParameters(parameters: ["phone": phone, "amount":amount], encoding: JSONEncoding.default)
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .patch
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        return [
            "Authorization": "Bearer \(token)",
        ]
    }
}
