//
//  TopupNetworkManager.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation

public protocol TopupNetworkManager {
    func topup(phone: String, amount: String, completion: @escaping (TopupResponse?, Error?) -> ())
}
