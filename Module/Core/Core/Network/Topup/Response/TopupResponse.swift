//
//  TopupResponse.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation

public struct TopupResponse: Codable {
    public var status: Int
    public var message: String
}
