//
//  RegisterNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Moya

public class RegisterNetworkManagerImpl:
    RegisterNetworkManager {
    
    public init() {
        
    }
    
    public func signup(username: String, email: String, password: String, completion: @escaping (RegisterResponse?, Error?) -> ()) {
        let provider = MoyaProvider<RegisterApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.signup(username: username, email: email, password: password)) { response in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let signupResponse = try decoder.decode(RegisterResponse.self, from: result.data)
                    completion(signupResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
