//
//  TransferNetworkManager.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation

public protocol TransferNetworkManager {
    func transfer(id: String, amount: String, notes: String, completion: @escaping (TransferResponse?, Error?) -> ())
}
