//
//  TransferApi.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Moya

public enum TransferApi {
    case postTransfer(id: String, amount: String, notes: String)
}

extension TransferApi: TargetType {
    public var path: String{
        switch self {
        case .postTransfer:
            return "/tranfer/newTranfer"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self{
        case .postTransfer(let id, let amount,let notes):
            return .requestParameters(parameters: ["receiver": id, "amount":amount, "notes":notes], encoding: JSONEncoding.default)
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .post
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        let pin: String = UserDefaultHelper.shared.get(key: .pinTransfer) ?? ""
        return [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(token)",
            "x-access-PIN": "\(pin)",
        ]
    }
}
