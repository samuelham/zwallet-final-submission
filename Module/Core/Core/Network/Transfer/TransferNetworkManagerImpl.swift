//
//  TransferNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Moya

public class TransferNetworkManagerImpl: TransferNetworkManager {
    
    public init() {
        
    }
    
    public func transfer(id: String, amount: String, notes: String, completion: @escaping (TransferResponse?, Error?) -> ()) {
        let provider = MoyaProvider<TransferApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.postTransfer(id: id, amount: amount, notes: notes)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let transferResponse = try decoder.decode(TransferResponse.self, from: result.data)
                    completion(transferResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
