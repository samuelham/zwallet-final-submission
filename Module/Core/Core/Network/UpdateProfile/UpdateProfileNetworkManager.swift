//
//  UpdateProfileNetworkManager.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public protocol UpdateProfileNetworkManager {
    func updateProfile(username: String, firstname: String, lastname: String, email: String, phone: String, completion: @escaping (UpdateProfileResponse?, Error?) -> ())
}
