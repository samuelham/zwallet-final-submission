//
//  UpdateProfileResponse.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation

public struct UpdateProfileResponse: Codable {
    public var status: Int
    public var message: String
    public var data: UpdateProfileDataResponse
}

public struct UpdateProfileDataResponse: Codable {
    public var username: String
    public var firstname: String
    public var lastname: String
    public var email: String
    public var phone: String
}
