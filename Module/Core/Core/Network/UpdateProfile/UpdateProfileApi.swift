//
//  UpdateProfileApi.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Moya

public enum UpdateProfileApi {
    case patchUpdateProfile(username: String, firstname: String, lastname: String, email: String, phone: String)
}

extension UpdateProfileApi: TargetType {
    public var path: String{
        switch self {
        case .patchUpdateProfile:
            return "/user/changeInfo"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self{
        case .patchUpdateProfile(let username, let firstname, let lastname, let email, let phone):
            return .requestParameters(parameters: ["username": username, "firstname":firstname, "lastname":lastname, "email":email, "phone":phone], encoding: JSONEncoding.default)
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .patch
    }
    
    public var headers: [String : String]? {
        let token: String = UserDefaultHelper.shared.get(key: .userToken) ?? ""
        return [
            "Content-Type": "application/json",
            "Authorization": "Bearer \(token)"
        ]
    }
}
