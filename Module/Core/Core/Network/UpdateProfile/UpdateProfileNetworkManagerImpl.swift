//
//  UpdateProfileNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Moya

public class UpdateProfileNetworkManagerImpl: UpdateProfileNetworkManager {
    
    public init() {
        
    }
    
    public func updateProfile(username: String, firstname: String, lastname: String, email: String, phone: String, completion: @escaping (UpdateProfileResponse?, Error?) -> ()) {
        
        let provider = MoyaProvider<UpdateProfileApi>(plugins:[NetworkLoggerPlugin(configuration: .init(logOptions: .verbose))])
        
        provider.request(.patchUpdateProfile(username: username, firstname: firstname, lastname: lastname, email: email, phone: phone)) { (response) in
            switch response {
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let updateProfileResponse = try decoder.decode(UpdateProfileResponse.self, from: result.data)
                    completion(updateProfileResponse, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
}
