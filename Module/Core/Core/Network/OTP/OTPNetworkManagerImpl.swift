//
//  OTPNetworkManagerImpl.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public class OTPNetworkManagerImpl: OTPNetworkManager{
    
    public init() {
        
    }
    
    public func getOTP(email: String, otp: String, completion: @escaping (GetOTPResponse?, Error?) -> ()) {
        let provider = MoyaProvider<OTPApi>()
        
        provider.request(.getOTPResponse(email: email, otp: otp)) { response in
            switch response{
            case .success(let result):
                let decoder = JSONDecoder()
                do {
                    let getOTPResponse = try decoder.decode(GetOTPResponse.self, from: result.data)
                    completion(getOTPResponse, nil)
                }
                catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
