//
//  OTPNetworkManager.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation

public protocol OTPNetworkManager {
    func getOTP(email: String, otp: String, completion: @escaping (GetOTPResponse?, Error?) -> ())
}
