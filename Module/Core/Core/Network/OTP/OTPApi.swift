//
//  OTPApi.swift
//  Core
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Moya

public enum OTPApi {
    case getOTPResponse(email: String, otp: String)
}

extension OTPApi: TargetType {
    public var path: String {
        switch self {
        case .getOTPResponse(let email, let otp):
            return "/auth/activate/\(email)/\(otp)"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        switch self {
        case .getOTPResponse:
            return .requestPlain
        }
    }
    
    public var baseURL: URL {
        return URL(string: AppConstant.baseUrl)!
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var headers: [String : String]? {
        return [
            "Content-Type": "application/json"
        ]
    }
}



