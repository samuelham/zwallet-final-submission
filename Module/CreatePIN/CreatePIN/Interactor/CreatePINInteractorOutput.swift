//
//  CreatePINInteractorOutput.swift
//  CreatePIN
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol CreatePINInteractorOutput {
    func CreatePINResponseResult(isSuccess: Bool)
}
