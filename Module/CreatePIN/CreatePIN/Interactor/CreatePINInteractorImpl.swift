//
//  CreatePINInteractorImpl.swift
//  CreatePIN
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

class CreatePINInteractorImpl: CreatePINInteractor{
    
    var interactorOutput: CreatePINInteractorOutput?
    let createPINNetworkManager: CreatePINNetworkManager
    
    init(createPINNetworkManager: CreatePINNetworkManager) {
        self.createPINNetworkManager = createPINNetworkManager
    }
    
    func getCreatePINResponse(pin: String) {
        self.createPINNetworkManager.getPIN(pin: pin) { (data, error) in
            if data?.status == 200 {
                self.interactorOutput?.CreatePINResponseResult(isSuccess: true)
            } else {
                self.interactorOutput?.CreatePINResponseResult(isSuccess: false)
            }
        }
    }
}
