//
//  CreatePINInteractor.swift
//  CreatePIN
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol CreatePINInteractor {
    func getCreatePINResponse(pin: String)
}
