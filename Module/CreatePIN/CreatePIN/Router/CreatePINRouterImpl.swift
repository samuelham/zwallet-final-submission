//
//  CreatePINRouterImpl.swift
//  CreatePIN
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class CreatePINRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.CreatePIN")
        let vc = CreatePINViewController(nibName: "CreatePINViewController", bundle: bundle)
        
        let networkManager = CreatePINNetworkManagerImpl()
        let router = CreatePINRouterImpl()
        
        let interactor = CreatePINInteractorImpl(createPINNetworkManager: networkManager)
        
        let presenter = CreatePINPresenterImpl(view: vc, router: router, interactor: interactor)

        interactor.interactorOutput = presenter

        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension CreatePINRouterImpl: CreatePINRouter{
    func navigateToPINSuccess() {
        AppRouter.shared.navigateToPINSuccess()
    }
}
