//
//  CreatePINRouter.swift
//  CreatePIN
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol CreatePINRouter {
    func navigateToPINSuccess()
}
