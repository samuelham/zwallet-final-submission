//
//  CreatePINPresenterImpl.swift
//  CreatePIN
//
//  Created by MacBook on 27/05/21.
//

import Foundation

class CreatePINPresenterImpl: CreatePINPresenter {
    
    let view: CreatePINView
    let interactor: CreatePINInteractor
    let router: CreatePINRouter
    
    init(view: CreatePINView, router: CreatePINRouter, interactor: CreatePINInteractor) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    func getCreatePINResponse(pin: String) {
        self.interactor.getCreatePINResponse(pin: pin)
    }
}

extension CreatePINPresenterImpl: CreatePINInteractorOutput {
    func CreatePINResponseResult(isSuccess: Bool) {
        if isSuccess {
            self.router.navigateToPINSuccess()
        } else {
            self.view.showError()
        }
    }
    
}
