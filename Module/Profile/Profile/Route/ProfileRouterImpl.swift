//
//  ProfileRouterImpl.swift
//  Profile
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core
import UIKit

public class ProfileRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.Profile")
        let vc = ProfileViewController(nibName: "ProfileViewController", bundle: bundle)
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
