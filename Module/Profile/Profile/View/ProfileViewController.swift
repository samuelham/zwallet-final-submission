//
//  ProfileViewController.swift
//  Profile
//
//  Created by MacBook on 30/05/21.
//

import UIKit
import Core
import Kingfisher

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var profileName: UILabel!
    
    @IBOutlet weak var profilePhone: UILabel!
    
    var presenter: ProfilePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let getProfileName: String = UserDefaultHelper.shared.get(key: .userName) ?? ""
        profileName.text = getProfileName
        
        let getProfilePhone: String = UserDefaultHelper.shared.get(key: .userPhone) ?? ""
        profilePhone.text = getProfilePhone
        
        let getProfileImage: String = UserDefaultHelper.shared.get(key: .userImageUrl) ?? ""
        let url = URL(string: getProfileImage)
        profileImg.kf.setImage(with: url)
        
        self.presenter?.getProfile()

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickToPersonalInfo(_ sender: Any) {
        AppRouter.shared.navigateToPersonal()
    }
    
    @IBAction func onClickBackToHome(_ sender: Any) {
        AppRouter.shared.navigateToHome()
    }
}
