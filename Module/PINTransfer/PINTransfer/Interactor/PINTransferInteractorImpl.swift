//
//  PINTransferInteractorImpl.swift
//  PINTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

class PINTransferInteractorImpl: PINTransferInteractor {
    
    let transferNetworkManager: TransferNetworkManager
    
    init(networkManager: TransferNetworkManager) {
        self.transferNetworkManager = networkManager
    }
    
    func transfer(id: String, amount: String, notes: String) {
        self.transferNetworkManager.transfer(id: id, amount: amount, notes: notes) { data, error in
            if let transferData = data {
                print(transferData.message)
                AppRouter.shared.navigateToSuccessTransfer()
            } else {
                print(data?.message ?? "")
                AppRouter.shared.navigateToFailedTransfer()
            }
        }
    }
}
