//
//  PINTransferInteractor.swift
//  PINTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PINTransferInteractor {
    func transfer(id: String, amount: String, notes: String)
}
