//
//  PINTransferPresenter.swift
//  PINTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PINTransferPresenter {
    func transfer(id: String, amount: String, notes: String)
}
