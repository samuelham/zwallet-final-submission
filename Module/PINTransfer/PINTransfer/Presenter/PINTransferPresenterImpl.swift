//
//  PINTransferPresenterImpl.swift
//  PINTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

class PINTransferPresenterImpl: PINTransferPresenter {
    
    let view: PINTransferView
    let interactor: PINTransferInteractor
    let router: PINTransferRouter
    
    init(view: PINTransferView, interactor: PINTransferInteractor, router: PINTransferRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func transfer(id: String, amount: String, notes: String) {
        self.interactor.transfer(id: id, amount: amount, notes: notes)
    }
}
