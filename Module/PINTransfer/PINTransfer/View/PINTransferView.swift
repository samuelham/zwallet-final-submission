//
//  PINTransferView.swift
//  PINTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol PINTransferView {
    func showError()
    func showSuccess()
}
