//
//  PINTransferViewController.swift
//  PINTransfer
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core

class PINTransferViewController: UIViewController {
    
    @IBOutlet weak var pinLabel1: UITextField!
    
    @IBOutlet weak var pinLabel2: UITextField!
    
    @IBOutlet weak var pinLabel3: UITextField!
    
    @IBOutlet weak var pinLabel4: UITextField!
    
    @IBOutlet weak var pinLabel5: UITextField!
    
    @IBOutlet weak var pinLabel6: UITextField!
    
    @IBOutlet weak var confirmTransferBtn: UIButton!
    
    var presenter: PINTransferPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pinLabel1.layer.borderWidth = 1.0
        pinLabel2.layer.borderWidth = 1.0
        pinLabel3.layer.borderWidth = 1.0
        pinLabel4.layer.borderWidth = 1.0
        pinLabel5.layer.borderWidth = 1.0
        pinLabel6.layer.borderWidth = 1.0
        
        if (pinLabel1.text == "" && pinLabel2.text == "" && pinLabel3.text == "" && pinLabel4.text == "" && pinLabel5.text == "" && pinLabel6.text == "") {
            confirmTransferBtn.isUserInteractionEnabled = false
            confirmTransferBtn.backgroundColor = UIColor.systemGray
            pinLabel1.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel2.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel3.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel4.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel5.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel6.layer.borderColor = UIColor.systemGray.cgColor
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickTFNow(_ sender: Any) {
        let getPin1: String = pinLabel1.text ?? ""
        let getPin2: String = pinLabel2.text ?? ""
        let getPin3: String = pinLabel3.text ?? ""
        let getPin4: String = pinLabel4.text ?? ""
        let getPin5: String = pinLabel5.text ?? ""
        let getPin6: String = pinLabel6.text ?? ""
        
        let collectPin: String = "\(getPin1)\(getPin2)\(getPin3)\(getPin4)\(getPin5)\(getPin6)"
        
        UserDefaultHelper.shared.set(key: .pinTransfer , value: collectPin)
        
        let getReceiverId: String = UserDefaultHelper.shared.get(key: .contactId) ?? ""
        let getAmount: String = UserDefaultHelper.shared.get(key: .amountTransfer) ?? ""
        let getNotes: String = UserDefaultHelper.shared.get(key: .notesTransfer) ?? ""
        
        self.presenter?.transfer(id: getReceiverId, amount: getAmount, notes: getNotes)
        print("onclick transfer vc")
    }
    
    @IBAction func textFieldValueChanged(_ sender: Any) {
        if (pinLabel1.text != "" && pinLabel2.text != "" && pinLabel3.text != "" && pinLabel4.text != "" && pinLabel5.text != "" && pinLabel6.text != "") {
            confirmTransferBtn.isUserInteractionEnabled = true
            confirmTransferBtn.backgroundColor = UIColor.systemBlue
            pinLabel1.layer.borderColor = UIColor.systemBlue.cgColor
            pinLabel2.layer.borderColor = UIColor.systemBlue.cgColor
            pinLabel3.layer.borderColor = UIColor.systemBlue.cgColor
            pinLabel4.layer.borderColor = UIColor.systemBlue.cgColor
            pinLabel5.layer.borderColor = UIColor.systemBlue.cgColor
            pinLabel6.layer.borderColor = UIColor.systemBlue.cgColor
        } else {
            confirmTransferBtn.isUserInteractionEnabled = false
            confirmTransferBtn.backgroundColor = UIColor.systemGray
            pinLabel1.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel2.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel3.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel4.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel5.layer.borderColor = UIColor.systemGray.cgColor
            pinLabel6.layer.borderColor = UIColor.systemGray.cgColor
        }
    }
    
    @IBAction func onClickBackConfirmation(_ sender: Any) {
        AppRouter.shared.navigateToConfirmTransfer()
    }
    
}

extension PINTransferViewController: PINTransferView {
    func showError() {
        let alert = UIAlertController(
        title: "Peringatan",
        message: "Transfer Anda Gagal",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSuccess() {
        let alert = UIAlertController(
        title: "Berhasil",
        message: "Tranfer Anda Berhasil",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
