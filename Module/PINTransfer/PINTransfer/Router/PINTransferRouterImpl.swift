//
//  PINTransferRouterImpl.swift
//  PINTransfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

public class PINTransferRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.PINTransfer")
        let vc = PINTransferViewController(nibName: "PINTransferViewController", bundle: bundle)
        
        let networkManager = TransferNetworkManagerImpl()
        
        let router = PINTransferRouterImpl()
        let interactor = PINTransferInteractorImpl(networkManager: networkManager)
        let presenter = PINTransferPresenterImpl(view: vc, interactor: interactor, router: router)
        
//        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension PINTransferRouterImpl: PINTransferRouter {
    func navigateToLogin() {
        AppRouter.shared.navigateToLogin()
    }
}
