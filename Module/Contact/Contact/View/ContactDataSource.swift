//
//  ContactDataSource.swift
//  Contact
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

class ContactDataSource: NSObject, UITableViewDataSource {
    
    var viewController: ContactViewController!
    
    var contact: [ContactEntity] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contact.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        cell.showData(contact: self.contact[indexPath.row])
        return cell
    }
}
