//
//  ContactCell.swift
//  Contact
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core
import Kingfisher

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var imageContactCell: UIImageView!
    
    @IBOutlet weak var nameContactCell: UILabel!
    
    @IBOutlet weak var phoneContactCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func showData(contact: ContactEntity){
        
        self.nameContactCell.text = contact.name
        self.phoneContactCell.text = contact.phone
        
        let url = URL(string: contact.image)
        self.imageContactCell.kf.setImage(with: url)
    }
    
}
