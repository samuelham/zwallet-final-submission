//
//  ContactViewController.swift
//  Contact
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core

class ContactViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var searchContactTF: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var countContact: UILabel!
    
    var dataSource = ContactDataSource()
    
    var presenter: ContactPresenter?
    
    var contact: [ContactEntity] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchContactTF.placeholder = "Search receiver here"
        self.setupTableView()
        self.presenter?.loadContact()
        
        countContact.text = String(Int(dataSource.contact.count)) + " Contact Found"
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        countContact.text = String(Int(dataSource.contact.count)) + " Contact Found"
    }

    @IBAction func onClickBackToHome(_ sender: Any) {
        AppRouter.shared.navigateToHome()
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "ContactCell", bundle: Bundle(identifier: "com.app.Contact")), forCellReuseIdentifier: "ContactCell")
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let data = dataSource.contact[indexPath.row]
        let setContactId: String = "\(data.id)"
        let setContactName: String = data.name
        let setContactPhone: String = data.phone
        let setContactImage: String = data.image
        
        UserDefaultHelper.shared.set(key: .contactId, value: setContactId)
        UserDefaultHelper.shared.set(key: .contactName, value: setContactName)
        UserDefaultHelper.shared.set(key: .contactPhone, value: setContactPhone)
        UserDefaultHelper.shared.set(key: .contactImage, value: setContactImage)
        
        print(setContactId)
        
        AppRouter.shared.navigateToCreateTransfer()
        
        print("On click contact to transfer")
    }
}

extension ContactViewController: ContactView {
    func showContactData(contact: [ContactEntity]) {
        self.countContact.text = String(contact.count) + " Contact Found"
        self.dataSource.contact = contact
        self.tableView.reloadData()
    }
}
