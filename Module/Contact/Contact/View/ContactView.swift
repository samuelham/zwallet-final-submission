//
//  ContactView.swift
//  Contact
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

protocol ContactView {
    func showContactData(contact: [ContactEntity])
}
