//
//  ContactRouter.swift
//  Contact
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol ContactRouter {
    func navigateToHome()
}
