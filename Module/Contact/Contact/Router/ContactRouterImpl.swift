//
//  ContactRouterImpl.swift
//  Contact
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core
import UIKit

public class ContactRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.Contact")
        let vc = ContactViewController(nibName: "ContactViewController", bundle: bundle)
        
        let contactNetworkManager = ContactNetworkManagerImpl()
        
        let router = ContactRouterImpl()
        let interactor = ContactInteractorImpl(contactNetworkManager:contactNetworkManager)
        let presenter = ContactPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension ContactRouterImpl: ContactRouter {
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
}
