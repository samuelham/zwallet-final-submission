//
//  ContactPresenterImpl.swift
//  Contact
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import UIKit
import Core

class ContactPresenterImpl: ContactPresenter {
    
    let view: ContactView
    let interactor: ContactInteractor
    let router: ContactRouter
    
    init(view: ContactView, interactor: ContactInteractor, router: ContactRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func loadContact() {
        self.interactor.getContact()
    }
}

extension ContactPresenterImpl: ContactInteractorOuput {
    func loadedContact(contact: [ContactEntity]) {
        self.view.showContactData(contact: contact)
    }
}
