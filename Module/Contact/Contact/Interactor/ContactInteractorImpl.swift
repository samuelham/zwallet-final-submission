//
//  ContactInteractorImpl.swift
//  Contact
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

class ContactInteractorImpl: ContactInteractor{
    
    var interactorOutput: ContactInteractorOuput?
    
    let contactNetworkManager: ContactNetworkManager
    
    init(contactNetworkManager: ContactNetworkManager) {
        self.contactNetworkManager = contactNetworkManager
    }
    
    func getContact() {
        self.contactNetworkManager.getContact { (data, error) in
            var contact: [ContactEntity] = []
            
            data?.forEach({ (contactData) in
                contact.append(ContactEntity(id: contactData.id, name: contactData.name, phone: contactData.phone, image: "\(AppConstant.baseUrl)\(contactData.image)"))
                self.interactorOutput?.loadedContact(contact: contact)
            })
        }
    }
}
