//
//  ContactInteractorOutput.swift
//  Contact
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core

protocol ContactInteractorOuput {
    func loadedContact(contact: [ContactEntity])
}
