//
//  ContactInteractor.swift
//  Contact
//
//  Created by MacBook on 28/05/21.
//

import Foundation

protocol ContactInteractor {
    func getContact()
}
