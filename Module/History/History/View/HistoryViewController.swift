//
//  HistoryViewController.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core

class HistoryViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = HistoryDataSource()
    
    var presenter: HistoryPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        self.presenter?.loadTransaction()

        // Do any additional setup after loading the view.
    }
    @IBAction func backHomeAction(_ sender: Any) {
        presenter?.showHome()
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.app.Core")), forCellReuseIdentifier: "TransactionCell")
        self.tableView.dataSource = self.dataSource
        self.tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 100
        default:
            return 80
        }
    }
}

extension HistoryViewController: HistoryView {
    
    func showTransactionData(transaction: [TransactionEntity]) {
        self.dataSource.transaction = transaction
        self.tableView.reloadData()
    }
}
