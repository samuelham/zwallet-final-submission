//
//  HistoryInteractorOutput.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import Core

protocol HistoryInteractorOuput {
    func loadedTransaction(transactions: [TransactionEntity])
}
