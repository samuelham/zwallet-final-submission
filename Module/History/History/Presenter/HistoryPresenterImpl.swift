//
//  HistoryPresenterImpl.swift
//  History
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit
import Core

class HistoryPresenterImpl: HistoryPresenter {
    
    let view: HistoryView
    let interactor: HistoryInteractor
    let router: HistoryRouter
    
    init(view: HistoryView, interactor: HistoryInteractor, router: HistoryRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func showHome() {
        self.router.navigateToHome()
    }
    
    func loadTransaction() {
        self.interactor.getTransaction()
    }
}

extension HistoryPresenterImpl: HistoryInteractorOuput {
    
    func loadedTransaction(transactions: [TransactionEntity]) {
        self.view.showTransactionData(transaction: transactions)
    }
}
