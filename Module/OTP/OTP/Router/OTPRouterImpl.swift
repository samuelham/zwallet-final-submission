//
//  OTPRouterImpl.swift
//  OTP
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class OTPRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.OTP")
        let vc = OTPViewController(nibName: "OTPViewController", bundle: bundle)
        
        let networkManager = OTPNetworkManagerImpl()
        let authNetworkManager = AuthNetworkManagerImpl()
        let router = OTPRouterImpl()
        
        let interactor = OTPInteractorImpl(otpNetworkManager: networkManager, authNetworkManager: authNetworkManager)
        
        let presenter = OTPPresenterImpl(view: vc, router: router, interactor: interactor)

        interactor.interactorOutput = presenter

        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension OTPRouterImpl: OTPRouter {
    func navigateToLogin() {
        AppRouter.shared.navigateToLogin()
    }
}
