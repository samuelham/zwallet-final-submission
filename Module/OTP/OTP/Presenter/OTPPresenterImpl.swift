//
//  OTPPresenterImpl.swift
//  OTP
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

class OTPPresenterImpl: OTPPresenter {
    
    let view: OTPView
    let interactor: OTPInteractor
    let router: OTPRouter
    
    init(view: OTPView, router: OTPRouter, interactor: OTPInteractor) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
    func getOTPResponse(email: String, otp: String) {
        self.interactor.getOTPResponse(email: email, otp: otp)
    }
    
    func loginAuto(email: String, password: String) {
        self.interactor.loginAuto(email: email, password: password)
    }
}

extension OTPPresenterImpl: OTPInteractorOutput {
    func OTPResponseResult(isSuccess: Bool) {
        if isSuccess {
//            let getEmail: String = UserDefaultHelper.shared.get(key: .email) ?? ""
//            let getPassword: String = UserDefaultHelper.shared.get(key: .password) ?? ""
//            self.interactor.loginAuto(email: getEmail, password: getPassword)
            self.router.navigateToLogin()
        } else {
            self.view.showError()
        }
    }
}
