//
//  OTPInteractor.swift
//  OTP
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol OTPInteractor {
    func getOTPResponse(email: String, otp: String)
    func loginAuto(email: String, password: String)
}
