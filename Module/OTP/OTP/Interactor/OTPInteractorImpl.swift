//
//  OTPInteractorImpl.swift
//  OTP
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import Core

class OTPInteractorImpl: OTPInteractor {
    
    
    var interactorOutput: OTPInteractorOutput?
    let otpNetworkManager: OTPNetworkManager
    let authNetworkManager: AuthNetworkManager
    
    init(otpNetworkManager: OTPNetworkManager, authNetworkManager: AuthNetworkManager) {
        self.otpNetworkManager = otpNetworkManager
        self.authNetworkManager = authNetworkManager
    }
    
    func getOTPResponse(email: String, otp: String) {
        self.otpNetworkManager.getOTP(email: email, otp: otp) { (data, error) in
            if data?.status == 200 {
//                AppRouter.shared.navigateToLogin()
                self.interactorOutput?.OTPResponseResult(isSuccess: true)
            } else {
                print("Error")
                self.interactorOutput?.OTPResponseResult(isSuccess: false)
            }
        }
    }
    
    
    func loginAuto(email: String, password: String) {
        self.authNetworkManager.login(email: email, password: password) { data, error in
            if let loginData = data {
                UserDefaultHelper.shared.set(key: .userToken, value: loginData.token)
                
            } else {
                print("Error")
            }
        }
    }
}
