//
//  OTPInteractorOutput.swift
//  OTP
//
//  Created by MacBook on 27/05/21.
//

import Foundation

protocol OTPInteractorOutput {
    func OTPResponseResult(isSuccess: Bool)
}
