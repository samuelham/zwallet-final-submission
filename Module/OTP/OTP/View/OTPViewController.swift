//
//  OTPViewController.swift
//  OTP
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core

class OTPViewController: UIViewController {
    
    @IBOutlet weak var OtpLabel1: UITextField!
    
    @IBOutlet weak var OtpLabel2: UITextField!
    
    @IBOutlet weak var OtpLabel3: UITextField!
    
    @IBOutlet weak var OtpLabel4: UITextField!
    
    @IBOutlet weak var OtpLabel5: UITextField!
    
    @IBOutlet weak var OtpLabel6: UITextField!
    
    @IBOutlet weak var confirmOtpBtn: UIButton!
    
    var presenter: OTPPresenter?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        OtpLabel1.layer.borderWidth = 1.0
        OtpLabel2.layer.borderWidth = 1.0
        OtpLabel3.layer.borderWidth = 1.0
        OtpLabel4.layer.borderWidth = 1.0
        OtpLabel5.layer.borderWidth = 1.0
        OtpLabel6.layer.borderWidth = 1.0
        
        if (OtpLabel1.text == "" && OtpLabel2.text == "" && OtpLabel3.text == "" && OtpLabel4.text == "" && OtpLabel5.text == "" && OtpLabel6.text == "") {
            confirmOtpBtn.isUserInteractionEnabled = false
            confirmOtpBtn.backgroundColor = UIColor.systemGray
            OtpLabel1.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel2.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel3.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel4.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel5.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel6.layer.borderColor = UIColor.systemGray.cgColor
        }

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickConfirmOtp(_ sender: Any) {
        let getOtp1: String = OtpLabel1.text ?? ""
        let getOtp2: String = OtpLabel2.text ?? ""
        let getOtp3: String = OtpLabel3.text ?? ""
        let getOtp4: String = OtpLabel4.text ?? ""
        let getOtp5: String = OtpLabel5.text ?? ""
        let getOtp6: String = OtpLabel6.text ?? ""
        
        let collectOtp: String = "\(getOtp1)\(getOtp2)\(getOtp3)\(getOtp4)\(getOtp5)\(getOtp6)"
        let getEmail: String = UserDefaultHelper.shared.get(key: .email) ?? ""
        
        self.presenter?.getOTPResponse(email: getEmail, otp: collectOtp)
    }
    
    @IBAction func textFieldValueChanged(_ sender: Any) {
        if (OtpLabel1.text != "" && OtpLabel2.text != "" && OtpLabel3.text != "" && OtpLabel4.text != "" && OtpLabel5.text != "" && OtpLabel6.text != "") {
            confirmOtpBtn.isUserInteractionEnabled = true
            confirmOtpBtn.backgroundColor = UIColor.systemBlue
            OtpLabel1.layer.borderColor = UIColor.systemBlue.cgColor
            OtpLabel2.layer.borderColor = UIColor.systemBlue.cgColor
            OtpLabel3.layer.borderColor = UIColor.systemBlue.cgColor
            OtpLabel4.layer.borderColor = UIColor.systemBlue.cgColor
            OtpLabel5.layer.borderColor = UIColor.systemBlue.cgColor
            OtpLabel6.layer.borderColor = UIColor.systemBlue.cgColor
        } else {
            confirmOtpBtn.isUserInteractionEnabled = false
            confirmOtpBtn.backgroundColor = UIColor.systemGray
            OtpLabel1.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel2.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel3.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel4.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel5.layer.borderColor = UIColor.systemGray.cgColor
            OtpLabel6.layer.borderColor = UIColor.systemGray.cgColor
        }
    }
}

extension OTPViewController: OTPView {
    func showError() {
        let alert = UIAlertController(
        title: "Peringatan",
        message: "Autentifikasi OTP Anda Gagal",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
