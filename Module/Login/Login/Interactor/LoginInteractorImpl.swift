//
//  LoginInteractorImpl.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import Foundation
import Core

class LoginInteractorImpl: LoginInteractor {
    
    var interactorOutput: LoginInteractorOutput?
    let authNetworkManager: AuthNetworkManager
    
    init(networkManager: AuthNetworkManager) {
        self.authNetworkManager = networkManager
    }
    
    func postLoginData(email: String, password: String) {
        self.authNetworkManager.login(email: email, password: password) { data, error in
            if data?.hasPin == true {
                UserDefaultHelper.shared.set(key: .userToken, value: data?.token)
                self.interactorOutput?.authenticationResult(isSuccess: true)
            } else {
                UserDefaultHelper.shared.set(key: .userToken, value: data?.token)
                self.interactorOutput?.authenticationResult(isSuccess: false)
            }
        }
    }
}
