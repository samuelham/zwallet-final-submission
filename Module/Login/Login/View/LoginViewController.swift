//
//  LoginViewController.swift
//  Login
//
//  Created by MacBook on 24/05/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var emailImg: UIImageView!
    
    @IBOutlet weak var passwordImg: UIImageView!
    
    var presenter: LoginPresenter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailText.placeholder = "Enter your e-mail"
        passwordText.placeholder = "Enter your password"
        
        formViewSetup()
        
        if (emailText.text!.isEmpty && passwordText.text!.isEmpty) {
            loginBtn.isUserInteractionEnabled = false
            loginBtn.backgroundColor = UIColor.gray
            }

        // Do any additional setup after loading the view.
    }
    
    var bottomBorderEmail = CALayer()
    var bottomBorderPassword = CALayer()
    func formViewSetup() {
        bottomBorderEmail.frame = CGRect(x: 0.0, y: emailView.frame.size.height-1, width: emailView.frame.width, height: 1.0)
        bottomBorderEmail.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        emailView.layer.addSublayer(bottomBorderEmail)
        
        bottomBorderPassword.frame = CGRect(x: 0.0, y: passwordView.frame.size.height-1, width: passwordView.frame.width, height: 1.0)
        bottomBorderPassword.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        passwordView.layer.addSublayer(bottomBorderPassword)
    }
    
    @IBAction func textFieldValueChanged(_ sender: Any){
        if emailText.text != "" && passwordText.text != "" {
            loginBtn.isUserInteractionEnabled = true
            loginBtn.backgroundColor = UIColor.systemBlue
            emailImg.tintColor = UIColor.systemBlue
            passwordImg.tintColor = UIColor.systemBlue
            self.bottomBorderEmail.backgroundColor = UIColor.systemBlue.cgColor
            self.bottomBorderPassword.backgroundColor = UIColor.systemBlue.cgColor
        } else {
            loginBtn.isUserInteractionEnabled = false
            loginBtn.backgroundColor = UIColor.gray
            self.bottomBorderEmail.backgroundColor = UIColor.systemGray.cgColor
            self.bottomBorderPassword.backgroundColor = UIColor.systemGray.cgColor
            emailImg.tintColor = UIColor.systemGray
            passwordImg.tintColor = UIColor.systemGray
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let email: String = emailText.text ?? ""
        let password: String = passwordText.text ?? ""
        
        self.presenter?.login(email: email, password: password)
    }
    
    @IBAction func signUpOnClick(_ sender: Any) {
        self.presenter?.showSignUpPage()
        print("tapped signup vc")
    }

}

extension LoginViewController: LoginView {
    func showError() {
        let alert = UIAlertController(
        title: "Peringatan",
        message: "Username atau password anda salah, silahkan coba lagi",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
