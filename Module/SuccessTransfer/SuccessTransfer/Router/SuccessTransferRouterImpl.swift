//
//  SuccessTransferRouterImpl.swift
//  SuccessTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core
import UIKit

public class SuccessTransferRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.SuccessTransfer")
        let vc = SuccessTransferViewController(nibName: "SuccessTransferViewController", bundle: bundle)
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
