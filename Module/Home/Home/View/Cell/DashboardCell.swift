//
//  DashboardCell.swift
//  Home
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Kingfisher
import Core

class DashboardCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var transferView: UIView!
    @IBOutlet weak var topUpView: UIView!
    
    @IBOutlet weak var toProfileView: UIView!
    var delegate: DashboardCellDelegate?
    
    override func awakeFromNib() {
        transferTopupUIViewConfig()
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func transferTopupUIViewConfig(){
        let transferGesture = UITapGestureRecognizer(target: self, action:  #selector(transferAction))
        transferView.addGestureRecognizer(transferGesture)
        
        
        let topupGesture = UITapGestureRecognizer(target: self, action:  #selector(topupAction))
        topUpView.addGestureRecognizer(topupGesture)
        
        let imageToProfileGesture = UITapGestureRecognizer(target: self, action:  #selector(toProfileAction))
        toProfileView.addGestureRecognizer(imageToProfileGesture)
    }
    
    @objc func transferAction(sender : UITapGestureRecognizer) {
        AppRouter.shared.navigateToContact()
        print("tapped transfer UIView")
    }
    
    @objc func topupAction(sender : UITapGestureRecognizer) {
        AppRouter.shared.navigateToTopup()
        print("tapped topup UIView")
    }
    
    @objc func toProfileAction(sender : UITapGestureRecognizer) {
        AppRouter.shared.navigateToProfile()
        print("tapped profile UIView")
    }

    func showData(userProfile: UserProfileEntity) {
        self.nameLabel.text = userProfile.name
        self.balanceLabel.text = userProfile.balance.formatToIdr()
        self.phoneNumberLabel.text = userProfile.phoneNumber
        
        UserDefaultHelper.shared.set(key: .currentBalance , value: String(userProfile.balance))
        UserDefaultHelper.shared.set(key: .userPhone , value: userProfile.phoneNumber)
        UserDefaultHelper.shared.set(key: .userName , value: userProfile.name)
        UserDefaultHelper.shared.set(key: .userImageUrl , value: userProfile.imageUrl)
        
        let url = URL(string: userProfile.imageUrl)
        self.profileImage.kf.setImage(with: url)
    }
    
    @IBAction func showTransactionAction(_ sender: Any) {
        self.delegate?.showAllTransaction()
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        self.delegate?.logout()
    }
}
