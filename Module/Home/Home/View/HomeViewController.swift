//
//  HomeViewController.swift
//  Home
//
//  Created by MacBook on 24/05/21.
//

import UIKit
import Core

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var dataSource = HomeDataSource()
    
    @IBOutlet weak var emptyStateView: UITextView!
    
    var presenter: HomePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        
        self.presenter?.loadProfile()
        self.presenter?.loadTransaction()
        
        tableView.separatorStyle = UITableViewCell.SeparatorStyle.none

        // Do any additional setup after loading the view.
    }
    
    func setupTableView() {
        self.dataSource.viewController = self
        
        self.tableView.register(UINib(nibName: "DashboardCell", bundle: Bundle(identifier: "com.app.Home")), forCellReuseIdentifier: "DashboardCell")
        self.tableView.register(UINib(nibName: "TransactionCell", bundle: Bundle(identifier: "com.app.Core")), forCellReuseIdentifier: "TransactionCell")
        
        self.tableView.dataSource = self.dataSource
    }
}

extension HomeViewController: DashboardCellDelegate {
    func showAllTransaction() {
        self.presenter?.showHistory(viewController: self)
    }
    
    func logout() {
        self.presenter?.logout()
    }
}

extension HomeViewController: HomeView {
    func showUserProfileData(userProfile: UserProfileEntity) {
        self.dataSource.userProfile = userProfile
        self.tableView.reloadData()
    }
    
    func showTransactionData(transaction: [TransactionEntity]) {
        self.dataSource.transaction = transaction
        self.tableView.reloadData()
    }
}
