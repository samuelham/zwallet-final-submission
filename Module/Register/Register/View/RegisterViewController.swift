//
//  RegisterViewController.swift
//  Register
//
//  Created by MacBook on 25/05/21.
//

import UIKit
import Core

class RegisterViewController: UIViewController {

    @IBOutlet weak var usernameLabel: UITextField!
    @IBOutlet weak var usernameIcon: UIImageView!
    
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var emailIcon: UIImageView!
    
    @IBOutlet weak var passwordLabel: UITextField!
    @IBOutlet weak var passwordIcon: UIImageView!
    
    @IBOutlet weak var usernameView: UIView!
    
    @IBOutlet weak var emailView: UIView!
    
    @IBOutlet weak var paswordView: UIView!
    
    @IBOutlet weak var signupBtn: UIButton!
    
    
    var presenter: RegisterPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formViewSetup()
        
        usernameLabel.placeholder = "Enter your username"
        emailLabel.placeholder = "Enter your e-mail"
        passwordLabel.placeholder = "Create your password"
        
        if (usernameLabel.text!.isEmpty && emailLabel.text!.isEmpty && passwordLabel.text!.isEmpty) {
            signupBtn.isUserInteractionEnabled = false
            signupBtn.backgroundColor = UIColor.gray
            }
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        let username: String = usernameLabel.text ?? ""
        let email: String = emailLabel.text ?? ""
        let password: String = passwordLabel.text ?? ""
        
        UserDefaultHelper.shared.set(key: .email , value: emailLabel.text)
        
        UserDefaultHelper.shared.set(key: .password , value: passwordLabel.text)
        
        self.presenter?.signup(username: username, email: email, password: password)
        
        print("tapped login vc")
    }
    
    var bottomBorderUsername = CALayer()
    var bottomBorderEmail = CALayer()
    var bottomBorderPassword = CALayer()
    func formViewSetup() {
        bottomBorderUsername.frame = CGRect(x: 0.0, y: usernameView.frame.size.height-1, width: usernameView.frame.width, height: 1.0)
        bottomBorderUsername.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        usernameView.layer.addSublayer(bottomBorderUsername)
        
        bottomBorderEmail.frame = CGRect(x: 0.0, y: emailView.frame.size.height-1, width: emailView.frame.width, height: 1.0)
        bottomBorderEmail.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        emailView.layer.addSublayer(bottomBorderEmail)
        
        bottomBorderPassword.frame = CGRect(x: 0.0, y: paswordView.frame.size.height-1, width: paswordView.frame.width, height: 1.0)
        bottomBorderPassword.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        paswordView.layer.addSublayer(bottomBorderPassword)
    }
    
    @IBAction func textFieldValueChanged(_ sender: Any){
        if usernameLabel.text != "" && emailLabel.text != "" && passwordLabel.text != "" {
            signupBtn.isUserInteractionEnabled = true
            signupBtn.backgroundColor = UIColor.systemBlue
            usernameIcon.tintColor = UIColor.systemBlue
            emailIcon.tintColor = UIColor.systemBlue
            passwordIcon.tintColor = UIColor.systemBlue
            self.bottomBorderUsername.backgroundColor = UIColor.systemBlue.cgColor
            self.bottomBorderEmail.backgroundColor = UIColor.systemBlue.cgColor
            self.bottomBorderPassword.backgroundColor = UIColor.systemBlue.cgColor
        } else {
            signupBtn.isUserInteractionEnabled = false
            signupBtn.backgroundColor = UIColor.gray
            self.bottomBorderUsername.backgroundColor = UIColor.systemGray.cgColor
            self.bottomBorderEmail.backgroundColor = UIColor.systemGray.cgColor
            self.bottomBorderPassword.backgroundColor = UIColor.systemGray.cgColor
            usernameIcon.tintColor = UIColor.systemGray
            emailIcon.tintColor = UIColor.systemGray
            passwordIcon.tintColor = UIColor.systemGray
        }
    }
    
    @IBAction func loginAction(_ sender: Any) {
        self.presenter?.showLoginPage()
        print("tapped login vc")
    }

}

extension RegisterViewController: RegisterView {
    func showError() {
        let alert = UIAlertController(
        title: "Peringatan",
        message: "Registrasi Anda Gagal",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSuccess() {
        let alert = UIAlertController(
        title: "Berhasil",
        message: "Registrasi Anda Berhasil",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
