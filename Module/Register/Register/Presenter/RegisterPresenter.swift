//
//  RegisterPresenter.swift
//  Register
//
//  Created by MacBook on 26/05/21.
//

import Foundation

protocol RegisterPresenter {
    func signup(username: String, email: String, password: String)
    func showLoginPage()
}
