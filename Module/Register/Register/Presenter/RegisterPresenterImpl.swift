//
//  RegisterPresenterImpl.swift
//  Register
//
//  Created by MacBook on 26/05/21.
//

import Foundation

class RegisterPresenterImpl: RegisterPresenter {
    
    let view: RegisterView
    let interactor: RegisterInteractor
    let router: RegisterRouter
    
    init(view: RegisterView, interactor: RegisterInteractor ,router: RegisterRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func signup(username: String, email: String, password: String) {
        self.interactor.postSignUpData(username: username, email: email, password: password)
        
        print("tapped register presenter")
    }
    
    func showLoginPage() {
        self.router.navigateToLogin()
        print("tapped login presenter")
    }
}

extension RegisterPresenterImpl: RegisterInteractorOutput{
    func registrationResult(isSuccess: Bool) {
        if isSuccess {
            self.router.navigateToOTP()
        } else {
            self.view.showError()
        }
    }
}
