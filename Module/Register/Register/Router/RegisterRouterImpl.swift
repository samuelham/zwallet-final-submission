//
//  RegisterRouterImpl.swift
//  Register
//
//  Created by MacBook on 25/05/21.
//

import Foundation
import UIKit
import Core

public class RegisterRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.Register")
        let vc = RegisterViewController(nibName: "RegisterViewController", bundle: bundle)
        
        let networkManager = RegisterNetworkManagerImpl()
        
        let router = RegisterRouterImpl()
        let interactor = RegisterInteractorImpl(networkManager: networkManager)
        let presenter = RegisterPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension RegisterRouterImpl: RegisterRouter {
    func navigateToOTP() {
        AppRouter.shared.navigateToOTP()
        print("tapped navigate to OTP")
    }
    
    func navigateToLogin() {
        AppRouter.shared.navigateToLogin()
        print("tapped navigate to Login")
    }
}


