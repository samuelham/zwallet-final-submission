//
//  RegisterInteractorImpl.swift
//  Register
//
//  Created by MacBook on 26/05/21.
//

import Foundation
import Core

class RegisterInteractorImpl: RegisterInteractor {
    
    var interactorOutput: RegisterInteractorOutput?
    let registerNetworkManager: RegisterNetworkManager
    
    init(networkManager: RegisterNetworkManager) {
        self.registerNetworkManager = networkManager
    }
    
    func postSignUpData(username: String, email: String, password: String) {
        self.registerNetworkManager.signup(username: username, email: email, password: password) { data, error in
            if let signupData = data {
                self.interactorOutput?.registrationResult(isSuccess: true)
                print(signupData.message)
//                AppRouter.shared.navigateToOTP()
            } else {
                self.interactorOutput?.registrationResult(isSuccess: false)
            }
        }
        
        print("tapped register interactor")
    }
}
