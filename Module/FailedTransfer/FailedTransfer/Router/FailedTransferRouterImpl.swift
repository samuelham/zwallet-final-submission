//
//  FailedTransferRouterImpl.swift
//  FailedTransfer
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core
import UIKit

public class FailedTransferRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.FailedTransfer")
        let vc = FailedTransferViewController(nibName: "FailedTransferViewController", bundle: bundle)
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
