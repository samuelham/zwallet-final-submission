//
//  FailedTransferViewController.swift
//  FailedTransfer
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import Core
import Kingfisher

class FailedTransferViewController: UIViewController {
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var balanceLeftLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var notesLabel: UILabel!
    
    @IBOutlet weak var contactImgView: UIImageView!
    
    @IBOutlet weak var contactNameLabel: UILabel!
    
    @IBOutlet weak var contactPhoneLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        labelDataSetup()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onClickBackToHome(_ sender: Any) {
        AppRouter.shared.navigateToHome()
//        AppRouter.shared.navigateToContact()
    }
    
    func labelDataSetup() {
        let getContactName: String = UserDefaultHelper.shared.get(key: .contactName) ?? ""
        contactNameLabel.text = getContactName
        
        let getContactPhone: String = UserDefaultHelper.shared.get(key: .contactPhone) ?? ""
        contactPhoneLabel.text = getContactPhone
        
        let getContactImage: String = UserDefaultHelper.shared.get(key: .contactImage) ?? ""
        let url = URL(string: getContactImage)
        contactImgView.kf.setImage(with: url)
        
        let getAmountTransfer: String = UserDefaultHelper.shared.get(key: .amountTransfer) ?? ""
        amountLabel.text = Int(getAmountTransfer)?.formatToIdr()
        
        let getCurrentBalance: String = UserDefaultHelper.shared.get(key: .currentBalance) ?? ""
        balanceLeftLabel.text = (Int(getCurrentBalance)! - Int(getAmountTransfer)!).formatToIdr()
        
        dateLabel.text = DateFormatter().DateNow()
        
        let getNotesTransfer: String = UserDefaultHelper.shared.get(key: .notesTransfer) ?? ""
        notesLabel.text = getNotesTransfer
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
