//
//  PINSuccessViewController.swift
//  PINSuccess
//
//  Created by MacBook on 27/05/21.
//

import UIKit
import Core

class PINSuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickLogin(_ sender: Any) {
        AppRouter.shared.navigateToHome()
    }
}
