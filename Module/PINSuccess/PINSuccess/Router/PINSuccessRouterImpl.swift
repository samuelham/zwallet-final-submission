//
//  PINSuccessRouterImpl.swift
//  PINSuccess
//
//  Created by MacBook on 27/05/21.
//

import Foundation
import UIKit
import Core

public class PINSuccessRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.PINSuccess")
        let vc = PINSuccessViewController(nibName: "PINSuccessViewController", bundle: bundle)
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension PINSuccessRouterImpl: PINSuccesRouter {
    func navigateToHome() {
        NotificationCenter.default.post(name: Notification.Name("reloadRootView"), object: nil)
    }
}
