//
//  TopupPresenterImpl.swift
//  Topup
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

class TopupPresenterImpl: TopupPresenter {
    
    let view: TopupView
    let interactor: TopupInteractor
    let router: TopupRouter
    
    init(view: TopupView, interactor: TopupInteractor, router: TopupRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func topup(phone: String, amount: String) {
        self.interactor.topup(phone: phone, amount: amount)
    }
}
