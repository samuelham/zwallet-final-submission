//
//  TopupInteractorImpl.swift
//  Topup
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import Core

class TopupInteractorImpl: TopupInteractor {
    
    let topupNetworkManager: TopupNetworkManager
    
    init(networkManager: TopupNetworkManager) {
        self.topupNetworkManager = networkManager
    }
    
    func topup(phone: String, amount: String) {
        self.topupNetworkManager.topup(phone: phone, amount: amount) { (data, error) in
            if let topupData = data {
//                self.interactorOutput?.registrationResult(isSuccess: true)
                print(topupData.message)
                AppRouter.shared.navigateToHome()
            } else {
                print(data?.message ?? "")
                AppRouter.shared.navigateToHome()
//                self.interactorOutput?.registrationResult(isSuccess: false)
//                AppRouter.shared.navigateToFailedTransfer()
            }
        }
    }
    
    
}
