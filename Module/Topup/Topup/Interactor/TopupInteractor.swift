//
//  TopupInteractor.swift
//  Topup
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol TopupInteractor {
    func topup(phone: String, amount: String)
}
