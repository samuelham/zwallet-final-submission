//
//  TopupViewController.swift
//  Topup
//
//  Created by MacBook on 29/05/21.
//

import UIKit
import Core

class TopupViewController: UIViewController {

    @IBOutlet weak var userPhoneNumber: UILabel!
    
    @IBOutlet weak var topupAmount: UITextField!
    
    @IBOutlet weak var topupButton: UIButton!
    
    
    var presenter: TopupPresenter?
    
    var getContactPhone: String = UserDefaultHelper.shared.get(key: .userPhone) ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topupAmount.placeholder = "Rp 0.00"
        userPhoneNumber.text = self.getContactPhone
        
        if (topupAmount.text!.isEmpty || getContactPhone.isEmpty) {
            topupButton.isUserInteractionEnabled = false
            topupButton.backgroundColor = UIColor.gray
            }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func textFieldValueChanged(_ sender: Any){
        if topupAmount.text != "" && getContactPhone != "" {
            topupButton.isUserInteractionEnabled = true
            topupButton.backgroundColor = UIColor.systemBlue
        } else {
            topupButton.isUserInteractionEnabled = false
            topupButton.backgroundColor = UIColor.gray
        }
    }

    @IBAction func onClickBackToHome(_ sender: Any) {
        AppRouter.shared.navigateToHome()
    }
    
    @IBAction func onClickTopupAction(_ sender: Any) {
        let setAmountTopup: String = topupAmount.text ?? ""
        
        self.presenter?.topup(phone: self.getContactPhone, amount: setAmountTopup)
    }
}

extension TopupViewController: TopupView {
    func showError() {
        let alert = UIAlertController(
        title: "Peringatan",
        message: "Topup Anda Gagal",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showSuccess() {
        let alert = UIAlertController(
        title: "Berhasil",
        message: "Topup Anda Berhasil",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
