//
//  TopupRouter.swift
//  Topup
//
//  Created by MacBook on 29/05/21.
//

import Foundation

protocol TopupRouter {
    func navigateToHome()
}
