//
//  TopupRouterImpl.swift
//  Topup
//
//  Created by MacBook on 29/05/21.
//

import Foundation
import UIKit
import Core

public class TopupRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.Topup")
        let vc = TopupViewController(nibName: "TopupViewController", bundle: bundle)
        
        let networkManager = TopupNetworkManagerImpl()
        
        let router = TopupRouterImpl()
        let interactor = TopupInteractorImpl(networkManager: networkManager)
        let presenter = TopupPresenterImpl(view: vc, interactor: interactor, router: router)
        
//        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension TopupRouterImpl: TopupRouter {
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
}
