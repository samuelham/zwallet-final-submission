//
//  PersonalEntity.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation

struct PersonalProfileEntity {
    var firstname: String
    var lastname: String
    var email: String
    var phone: String
    var image: String
}
