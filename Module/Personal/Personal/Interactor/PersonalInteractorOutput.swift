//
//  PersonalInteractorOutput.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core

protocol PersonalInteractorOutput {
    func getProfile(dataProfile: PersonalProfileEntity)
}
