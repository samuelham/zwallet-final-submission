//
//  PersonalInteractorImpl.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core

class PersonalInteractorImpl: PersonalInteractor {
    
    var interactorOutput: PersonalInteractorOutput?
    let profileNetworkManager: ProfileNetworkManager
    let updateProfileNetworkManager: UpdateProfileNetworkManager
    
    init(profileNetworkManager: ProfileNetworkManager, updateProfileNetworkManager: UpdateProfileNetworkManager) {
        self.profileNetworkManager = profileNetworkManager
        self.updateProfileNetworkManager = updateProfileNetworkManager
    }
    
    func getProfile() {
        self.profileNetworkManager.getProfile { data, error in
            if let profile = data {
                print("data didapatkan")
                let dataProfile = PersonalProfileEntity(firstname: profile.firstname, lastname: profile.lastname, email: profile.email, phone: profile.phone, image: "\(AppConstant.baseUrl)\(profile.image)")
                self.interactorOutput?.getProfile(dataProfile: dataProfile)
            }
            else {
                print("data kosong")
            }
        }
    }
    
    func updateProfile(username: String, firstname: String, lastname: String, email: String, phone: String) {
        self.updateProfileNetworkManager.updateProfile(username: username, firstname: firstname, lastname: lastname, email: email, phone: phone) { data, error in
            if let updateProfileData = data {
//                self.interactorOutput?.registrationResult(isSuccess: true)
//                print(updateProfileData.message)
                AppRouter.shared.navigateToHome()
            } else {
//                print(data?.message ?? "")
                AppRouter.shared.navigateToProfile()
//                self.interactorOutput?.registrationResult(isSuccess: false)
//                AppRouter.shared.navigateToFailedTransfer()
            }
        }
    }
}
