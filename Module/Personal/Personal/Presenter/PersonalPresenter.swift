//
//  PersonalPresenter.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation

protocol PersonalPresenter {
    func getProfile()
    func updateProfile(username: String, firstname: String, lastname: String, email: String, phone: String)
}
