//
//  PersonalPresenterImpl.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import UIKit
import Core

class PersonalPresenterImpl: PersonalPresenter {
    
    let view: PersonalView
    let interactor: PersonalInteractor
    let router: PersonalRouter
    
    init(view: PersonalView, interactor: PersonalInteractor, router: PersonalRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
    func getProfile() {
        self.interactor.getProfile()
    }
    
    func updateProfile(username: String, firstname: String, lastname: String, email: String, phone: String) {
        self.interactor.updateProfile(username: username, firstname: firstname, lastname: lastname, email: email, phone: phone)
    }
}

extension PersonalPresenterImpl: PersonalInteractorOutput {
    func getProfile(dataProfile: PersonalProfileEntity) {
        self.view.getProfile(dataProfile: dataProfile)
        print("Print get profile presenter")
    }
}
