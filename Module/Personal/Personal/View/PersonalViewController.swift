//
//  PersonalViewController.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import UIKit
import Core

class PersonalViewController: UIViewController {
    
    @IBOutlet weak var profileUsername: UITextField!
    
    @IBOutlet weak var profileFirstName: UITextField!
    
    @IBOutlet weak var profileLastName: UITextField!
    
    @IBOutlet weak var profileEmail: UITextField!
    
    @IBOutlet weak var profilePhoneNumber: UITextField!
    
    var presenter: PersonalPresenter?
    
    var dataProfile: PersonalProfileEntity = PersonalProfileEntity(firstname: "", lastname: "", email: "", phone: "", image: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.getProfile()
        setFormPlaceholder()
        
    }
    
    func setFormPlaceholder() {
        self.profileUsername.placeholder = "Username empty / not set"
        self.profilePhoneNumber.placeholder = "Phone number empty / not set"
        self.profileFirstName.placeholder = "Firstname empty / not set"
        self.profileLastName.placeholder = "Lastname empty / not set"
        self.profileEmail.placeholder = "Email empty / not set"
    }
    
    @IBAction func onClickBackToProfile(_ sender: Any) {
        AppRouter.shared.navigateToProfile()
    }
    
    @IBAction func onClickUpdatePersonalInfo(_ sender: Any) {
        let setUsername: String = profileUsername.text ?? ""
        let setFirstname: String = profileFirstName.text ?? ""
        let setLastname: String = profileLastName.text ?? ""
        let setEmail: String = profileEmail.text ?? ""
        let setPhone: String = profilePhoneNumber.text ?? ""
        
        self.presenter?.updateProfile(username: setUsername, firstname: setFirstname, lastname: setLastname, email: setEmail, phone: setPhone)
        print("onclick update profile vc")
    }
    

}

extension PersonalViewController: PersonalView{
    func getProfile(dataProfile: PersonalProfileEntity) {
        self.dataProfile = dataProfile
        self.profileUsername.text = dataProfile.firstname + dataProfile.lastname
        self.profileFirstName.text = dataProfile.firstname
        self.profileLastName.text = dataProfile.lastname
        self.profileEmail.text = dataProfile.email
        self.profilePhoneNumber.text = dataProfile.phone
        print("hit get Profile")
    }
}
