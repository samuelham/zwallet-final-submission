//
//  PersonalView.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import Core

protocol PersonalView {
    func getProfile(dataProfile: PersonalProfileEntity)
}
