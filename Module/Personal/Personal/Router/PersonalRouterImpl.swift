//
//  PersonalRouterImpl.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation
import UIKit
import Core

public class PersonalRouterImpl {
    
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.Personal")
        let vc = PersonalViewController(nibName: "PersonalViewController", bundle: bundle)
        
        let profileNetworkManager = ProfileNetworkManagerImpl()
        let updateProfileNetworkManager = UpdateProfileNetworkManagerImpl()
        
        let router = PersonalRouterImpl()
        let interactor = PersonalInteractorImpl(profileNetworkManager: profileNetworkManager, updateProfileNetworkManager: updateProfileNetworkManager)
        let presenter = PersonalPresenterImpl(view: vc, interactor: interactor, router: router)
        
        interactor.interactorOutput = presenter
        
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension PersonalRouterImpl: PersonalRouter {
    
    func navigateToHome() {
        AppRouter.shared.navigateToHome()
    }
}
