//
//  PersonalRouter.swift
//  Personal
//
//  Created by MacBook on 30/05/21.
//

import Foundation

protocol PersonalRouter {
    func navigateToHome()
}
