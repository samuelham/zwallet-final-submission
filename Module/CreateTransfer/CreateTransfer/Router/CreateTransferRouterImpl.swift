//
//  CreateTransferRouterImpl.swift
//  CreateTransfer
//
//  Created by MacBook on 28/05/21.
//

import Foundation
import Core
import UIKit

public class CreateTransferRouterImpl {
    public static func navigateToModule() {
        let bundle = Bundle(identifier: "com.app.CreateTransfer")
        let vc = CreateTransferViewController(nibName: "CreateTransferViewController", bundle: bundle)
        
        let router = CreateTransferRouterImpl()
        let presenter = CreateTransferPresenterImpl(view: vc, router: router)
            
        vc.presenter = presenter
        
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}

extension CreateTransferRouterImpl: CreateTransferRouter {
    func navigateToConfirmTransfer() {
        AppRouter.shared.navigateToConfirmTransfer()
    }
}
