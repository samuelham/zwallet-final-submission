//
//  CreateTransferPresenterImpl.swift
//  CreateTransfer
//
//  Created by MacBook on 31/05/21.
//

import Foundation
import UIKit
import Core

class CreateTransferPresenterImpl: CreateTransferPresenter {
    
    let view: CreateTransferView
    let router: CreateTransferRouter
    
    init(view: CreateTransferView, router: CreateTransferRouter) {
        self.view = view
        self.router = router
    }
    
    func navigateToConfirmTransfer() {
        self.router.navigateToConfirmTransfer()
    }
}
