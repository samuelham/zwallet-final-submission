//
//  CreateTransferViewController.swift
//  CreateTransfer
//
//  Created by MacBook on 28/05/21.
//

import UIKit
import Core
import Kingfisher

class CreateTransferViewController: UIViewController {
    
    @IBOutlet weak var imgViewTF: UIImageView!
    
    @IBOutlet weak var nameLabelTF: UILabel!
    
    @IBOutlet weak var phoneLabelTF: UILabel!
    
    @IBOutlet weak var amountTransfer: UITextField!
    
    @IBOutlet weak var balanceLabel: UILabel!
    
    @IBOutlet weak var notesTransfer: UITextField!
    
    @IBOutlet weak var notesTransferView: UIView!
    
    @IBOutlet weak var notesIcon: UIImageView!
    
    @IBOutlet weak var continueTFBtn: UIButton!
    
    var presenter: CreateTransferPresenter?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formViewSetup()
        
        amountTransfer.placeholder = "Rp 0.00"
        notesTransfer.placeholder = "Add some notes to transfer"
        
        if (amountTransfer.text!.isEmpty && notesTransfer.text!.isEmpty) {
            continueTFBtn.isUserInteractionEnabled = false
            continueTFBtn.backgroundColor = UIColor.gray
            }
        
        let getContactName: String = UserDefaultHelper.shared.get(key: .contactName) ?? ""
        nameLabelTF.text = getContactName
        
        let getContactPhone: String = UserDefaultHelper.shared.get(key: .contactPhone) ?? ""
        phoneLabelTF.text = getContactPhone
        
        let getCurrentBalance: String = UserDefaultHelper.shared.get(key: .currentBalance) ?? ""
        balanceLabel.text = (Int(getCurrentBalance)?.formatToIdr())! + " Available"
        
        let getContactImage: String = UserDefaultHelper.shared.get(key: .contactImage) ?? ""
        let url = URL(string: getContactImage)
        imgViewTF.kf.setImage(with: url)

        // Do any additional setup after loading the view.
    }
    
    var bottomBorderNotes = CALayer()
    func formViewSetup() {
        bottomBorderNotes.frame = CGRect(x: 0.0, y: notesTransferView.frame.size.height-1, width: notesTransferView.frame.width, height: 1.0)
        bottomBorderNotes.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            notesTransferView.layer.addSublayer(bottomBorderNotes)
    }
    
    @IBAction func onClickToContact(_ sender: Any) {
        AppRouter.shared.navigateToContact()
    }
    
    @IBAction func textFieldValueChanged(_ sender: Any){
        if amountTransfer.text != "" && notesTransfer.text != "" {
            continueTFBtn.isUserInteractionEnabled = true
            continueTFBtn.backgroundColor = UIColor.systemBlue
            notesIcon.tintColor = UIColor.systemBlue
            self.bottomBorderNotes.backgroundColor = UIColor.systemBlue.cgColor
        } else {
            continueTFBtn.isUserInteractionEnabled = false
            continueTFBtn.backgroundColor = UIColor.gray
            notesIcon.tintColor = UIColor.systemGray
            self.bottomBorderNotes.backgroundColor = UIColor.systemGray.cgColor
        }
    }
    
    @IBAction func onClickToConfirm(_ sender: Any) {
        let setAmountTransfer: String = amountTransfer.text ?? ""
        UserDefaultHelper.shared.set(key: .amountTransfer , value: setAmountTransfer)
        let setNotesTransfer: String = notesTransfer.text ?? ""
        UserDefaultHelper.shared.set(key: .notesTransfer , value: setNotesTransfer)
        
        self.presenter?.navigateToConfirmTransfer()
    }
}

extension CreateTransferViewController: CreateTransferView {
    func showError() {
        let alert = UIAlertController(
        title: "Peringatan",
        message: "Transfer Gagal",
        preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    }
}
