//
//  CreateTransferView.swift
//  CreateTransfer
//
//  Created by MacBook on 31/05/21.
//

import Foundation

protocol CreateTransferView {
    func showError()
}
